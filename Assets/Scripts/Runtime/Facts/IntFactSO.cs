using UnityEngine;

[CreateAssetMenu(fileName = "IntFactSO", menuName = "Blackboard/Facts/Int")]
public class IntFactSO : FactSO
{
    public int value;
    
    private void OnEnable()
    {
        factType = FactType.Int;
    }
}