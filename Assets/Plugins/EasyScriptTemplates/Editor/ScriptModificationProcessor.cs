using System.IO;
using UnityEditor;

public class ScriptModificationProcessor  : AssetModificationProcessor
{
    static void OnWillCreateAsset(string pathToNewFile)
    {
        pathToNewFile = pathToNewFile.Replace(".meta", "");
        
        FileInfo fileInfo = new FileInfo(pathToNewFile);
        
        string nameOfScript = Path.GetFileNameWithoutExtension(fileInfo.Name);
        string extension = Path.GetExtension(fileInfo.Name); 

        if(extension != ".cs") 
            return;
        
        string text = File.ReadAllText(pathToNewFile);
        text = text.Replace("#SCRIPTNAMEWITHOUTEDITOR#", nameOfScript.Replace("Editor", ""));
        
        File.WriteAllText(pathToNewFile, text);
        AssetDatabase.Refresh();
    }
}