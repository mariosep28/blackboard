using UnityEngine;

[CreateAssetMenu(fileName = "FloatFactSO", menuName = "Blackboard/Facts/Float")]
public class FloatFactSO : FactSO
{
    public float value;
    
    private void OnEnable()
    {
        factType = FactType.Float;
    }
}