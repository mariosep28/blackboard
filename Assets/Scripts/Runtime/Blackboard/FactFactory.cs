using System;
using UnityEngine;

public static class FactFactory
{ 
    public static FactSO CreateFact(FactType type)
    {
        FactSO newFact = type switch
        {
            FactType.Bool => ScriptableObject.CreateInstance<BoolFactSO>(),
            FactType.Int => ScriptableObject.CreateInstance<IntFactSO>(),
            FactType.Float => ScriptableObject.CreateInstance<FloatFactSO>(),
            FactType.String => ScriptableObject.CreateInstance<StringFactSO>(),
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
        };

        return newFact;
    }
}