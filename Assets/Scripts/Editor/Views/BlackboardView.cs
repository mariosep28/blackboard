using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class BlackboardView : VisualElement
{
    public new class UxmlFactory : UxmlFactory<BlackboardView, UxmlTraits> { }
    
    private readonly string uxmlPath = "UXML/Blackboard.uxml";

    private BlackboardSO _blackboard;
    
    private BlackboardListView _blackboardList;
    private Button _addFactButton;
    private Button _removeFactButton;
    
    public BlackboardView()
    {
        string path = Path.Combine(BlackboardEditorWindow.RelativePath, uxmlPath);
        VisualTreeAsset uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(path);
        uxml.CloneTree(this);
        
        // Get references
        _blackboardList = this.Q<BlackboardListView>();
        _addFactButton = this.Q<Button>("add-fact__button");
        _removeFactButton = this.Q<Button>("remove-fact__button");
        
        RegisterCallbacks();
    }
    
    private void RegisterCallbacks()
    {
        _addFactButton.clicked += OnAddFactButtonClicked;
        _removeFactButton.clicked += OnRemoveFactButtonClicked;
        _blackboardList.onFactTypeChanged += OnFactTypeChanged;
        FactPopUpMenu.onPopupMenuOptionSelected += AddFact;
        
        RegisterCallback<DetachFromPanelEvent>(_ => UnregisterCallbacks());
    }
    private void UnregisterCallbacks()
    {
        _addFactButton.clicked -= OnAddFactButtonClicked;
        _removeFactButton.clicked -= OnRemoveFactButtonClicked;
        _blackboardList.onFactTypeChanged -= OnFactTypeChanged;
        FactPopUpMenu.onPopupMenuOptionSelected -= AddFact;
        
    }
    
    public void PopulateView(BlackboardSO blackboard)
    {
        _blackboard = blackboard;
        
        _blackboardList.Populate(_blackboard);
    }
    
    private void AddFact(FactType factType)
    {
        FactSO newFact = FactFactory.CreateFact(factType);
        ScriptableObjectUtility.Save(newFact, $"Assets/SO/Facts/fact.asset", true);
        
        _blackboardList.Add(newFact);
    }

    private void RemoveFact(params FactSO[] facts)
    {
        _blackboardList.Remove(facts);
        ScriptableObjectUtility.Delete(facts);
    }
    
    private void OnAddFactButtonClicked()
    {
        FactPopUpMenu.DisplayAddFactPopupMenu();
    }
    
    private void OnRemoveFactButtonClicked()
    {
        FactSO[] factsSelected = _blackboardList.factsSelected;
        
        if(factsSelected.Length == 0 && _blackboard.facts.Count > 0)
            factsSelected = new [] { _blackboard.facts.Last() };
        
        if(factsSelected.Length > 0)
        {
            RemoveFact(factsSelected);
        }
    }

    private void OnFactTypeChanged(int i, FactType newType)
    {
        FactSO factToReplace = _blackboard.facts[i];

        if(newType == factToReplace.factType)
            return;
        
        FactSO newFact = FactFactory.CreateFact(newType);
        newFact.id = factToReplace.id;

        string path = AssetDatabase.GetAssetPath(factToReplace);
        ScriptableObjectUtility.Save(newFact, path, false);

        _blackboardList.Replace(i, newFact);
    }
}