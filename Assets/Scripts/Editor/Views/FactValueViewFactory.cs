using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

public static class FactValueViewFactory
{
    public static VisualElement CreateValueView(FactSO fact)
    {
        var so = new SerializedObject(fact);

        return fact switch
        {
            BoolFactSO => CreateToggle(so),
            IntFactSO => CreateIntegerField(so),
            FloatFactSO => CreateFloatField(so),
            StringFactSO => CreateTextField(so),
            _ => new VisualElement()
        };
    }
    
    public static TextField CreateTextField(SerializedObject serializedObject)
    {
        var valueProperty = serializedObject.FindProperty("value");
        
        var textField = new TextField("");
        textField.BindProperty(valueProperty);

        return textField;
    }
    
    public static IntegerField CreateIntegerField(SerializedObject serializedObject)
    {
        var valueProperty = serializedObject.FindProperty("value");
        
        var integerField = new IntegerField("");
        integerField.BindProperty(valueProperty);

        return integerField;
    }
    
    public static FloatField CreateFloatField(SerializedObject serializedObject)
    {
        var valueProperty = serializedObject.FindProperty("value");
        
        var floatField = new FloatField("");
        floatField.BindProperty(valueProperty);

        return floatField;
    }
    
    public static Toggle CreateToggle(SerializedObject serializedObject)
    {
        var valueProperty = serializedObject.FindProperty("value");
        
        var toggle = new Toggle("");
        toggle.BindProperty(valueProperty);

        return toggle;
    }
}