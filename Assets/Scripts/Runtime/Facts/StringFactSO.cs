using UnityEngine;

[CreateAssetMenu(fileName = "StringFactSO", menuName = "Blackboard/Facts/String")]
public class StringFactSO : FactSO
{
    public string value;
    
    private void OnEnable()
    {
        factType = FactType.String;
    }
}