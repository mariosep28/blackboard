using UnityEngine;

public abstract class FactSO : ScriptableObject
{
    public string id;
    public FactType factType;
}