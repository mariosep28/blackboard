using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

public class BlackboardListView : VisualElement
{
    public new class UxmlFactory : UxmlFactory<BlackboardListView, UxmlTraits> { }

    private readonly string uxmlPath = "UXML/BlackboardListView.uxml";

    public Action<int, FactType> onFactTypeChanged; 
    
    private MultiColumnListView _listView;
    private BlackboardSO _blackboard;
    private SerializedProperty _factsProperty;
    private List<FactSO> _facts => _blackboard.facts;

    public FactSO[] factsSelected => _listView.selectedItems.Cast<FactSO>().ToArray();
    
    public BlackboardListView()
    {
        string path = Path.Combine(BlackboardEditorWindow.RelativePath, uxmlPath);
        VisualTreeAsset uxml = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(path);
        uxml.CloneTree(this);
        
        _listView = this.Q<MultiColumnListView>();
        
        Setup();
    }

    private void Setup()
    {
        _listView.columns["name"].makeCell = () => MakeNameCell();
        _listView.columns["value"].makeCell = () => MakeCell();
        _listView.columns["type"].makeCell = () => MakeTypeCell();
        
        _listView.columns["name"].bindCell = (element, i) => BindName(element, new SerializedObject(_facts[i]));
        _listView.columns["value"].bindCell = (element, i) => BindValue(element, _facts[i]);
        _listView.columns["type"].bindCell = (element, i) => BindType(element, i);
    }
    
    public void Populate(BlackboardSO blackboard)
    {
        _blackboard = blackboard;
        _listView.itemsSource = _facts;
    }

    #region Modify list
    public void Add(FactSO fact)
    {
        _listView.itemsSource.Add(fact);
        _listView.RefreshItems();
    }
    
    public void Remove(params FactSO[] facts)
    {
        foreach (FactSO fact in facts)
            _listView.itemsSource.Remove(fact);    
        
        _listView.RefreshItems();
    }

    public void Replace(int i, FactSO fact)
    {
        _listView.itemsSource[i] = fact;
        _listView.RefreshItem(i);
    }
    #endregion

    #region Make
    private VisualElement MakeCell()
    {
        var cell = new VisualElement();
        cell.AddToClassList("centered");
        cell.style.paddingTop = 5f;
        cell.style.paddingBottom = 5f;
        
        return cell;
    }
    
    private VisualElement MakeNameCell()
    {
        var cell = MakeCell();
        
        var nameField = new TextField("");
        cell.Add(nameField);

        return cell;
    }
    
    private VisualElement MakeTypeCell()
    {
        var cell = MakeCell();
        
        var enumField = new EnumField(FactType.Bool);
        cell.Add(enumField);

        return cell;
    }
    #endregion
    
    #region Bind
    private void BindName(VisualElement cell, SerializedObject serializedObject)
    {
        TextField nameField = cell.Q<TextField>();
        
        var idProperty = serializedObject.FindProperty("id");
        nameField.BindProperty(idProperty);
    }
    
    private void BindValue(VisualElement cell, FactSO fact)
    {
        if(cell.childCount > 0)
            cell.RemoveAt(0);
        
        VisualElement content = FactValueViewFactory.CreateValueView(fact);
        cell.Add(content);
    }
    
    private void BindType(VisualElement cell, int i)
    {
        EnumField typeField = cell.Q<EnumField>();
        
        typeField.value = _facts[i].factType;
        typeField.RegisterValueChangedCallback(e => onFactTypeChanged?.Invoke(i, (FactType)e.newValue));
    }
    #endregion
}