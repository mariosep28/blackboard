using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

public class BlackboardEditorWindow : EditorWindow
{
    [SerializeField] private VisualTreeAsset vt;

    private BlackboardSO _blackboardSelected;
    private BlackboardView _blackboardView;

    public static string RelativePath => AssetDataBaseExtensions.GetDirectoryOfScript<BlackboardEditorWindow>();
    
    [MenuItem("Blackboard/Editor")]
    public static void OpenWindow()
    {
        BlackboardEditorWindow wnd = GetWindow<BlackboardEditorWindow>("Blackboard Editor");
        wnd.minSize = new Vector2(400, 400);
    }

    [OnOpenAsset]
    public static bool OnOpenAsset(int instanceID, int line)
    {
        if (Selection.activeObject is not BlackboardSO) 
            return false;
        OpenWindow();
        return true;
    }
    
    public void CreateGUI()
    {
        vt.CloneTree(rootVisualElement);

        OnSelectionChange();
    }

    private void OnSelectionChange()
    {
        BlackboardSO blackboard = Selection.activeObject as BlackboardSO;

        if (_blackboardSelected != null)
        {
            EditorUtility.SetDirty(_blackboardSelected);
            AssetDatabase.SaveAssets();
        }
        
        if (blackboard != null)
        {
            if (_blackboardView != null)
            {
                rootVisualElement.Remove(_blackboardView);
            }

            _blackboardSelected = blackboard;
            _blackboardView = new BlackboardView();
            
            SerializedObject so = new SerializedObject(blackboard);
            rootVisualElement.Bind(so);
            
            _blackboardView.PopulateView(blackboard);
            
            rootVisualElement.Add(_blackboardView);
        }
    }

    private void OnDestroy()
    {
        if (_blackboardSelected != null)
        {
            EditorUtility.SetDirty(_blackboardSelected);
            AssetDatabase.SaveAssets();
        }
    }
}