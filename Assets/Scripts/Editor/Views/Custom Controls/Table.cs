using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine.UIElements;

public class Table : VisualElement
{
    private readonly string vtPath = "UXML/Table.uxml";

    private List<TableColumn> _columns;
    private int _numColumns;
    private int _numRows;
    
    private VisualElement _table;
    private VisualElement _tableHeader;

    public List<TableColumn> Columns => _columns;
    public int NumColumns => _numColumns;
    public int NumRows => _numRows;
    
    
    public Table(List<TableColumn> columns)
    {
        string path = Path.Combine(BlackboardEditorWindow.RelativePath, vtPath);
        VisualTreeAsset vt = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(path);
        vt.CloneTree(this);

        _columns = columns;
        
        GetReferences();

        _numColumns = columns.Count;
        
        foreach (TableColumn column in columns)
        {
            TableCell tableCell = new TableCell(new Label(column.headerTitle), column.expandable, column.minWidth);
            
            _tableHeader.Add(tableCell);
        }
    }

    private void GetReferences()
    {
        _table = this.Q<VisualElement>("table");
        _tableHeader = this.Q<VisualElement>("table-header");
    }
    
    public void AddRow(VisualElement row)
    {
        InsertRow(_numRows, row);
    }

    public void InsertRow(int rowIndex, VisualElement row)
    {
        if(row.childCount != _numColumns || rowIndex > _numRows)
            return;

        TableRow tableRow = new TableRow(row, _columns);

        _table.Insert(rowIndex + 1, tableRow);
        
        _numRows++;
    }

    public void RemoveRow(int rowIndexToDelete)
    {
        if(rowIndexToDelete > _numRows || rowIndexToDelete < 0)
            return;
        
        _table.RemoveAt(rowIndexToDelete + 1);
        
        _numRows--;
    }
}

public struct TableColumn
{
    public string headerTitle;
    public bool expandable;
    public int? minWidth;

    public TableColumn(string headerTitle, bool expandable, int? minWidth = null)
    {
        this.headerTitle = headerTitle;
        this.expandable = expandable;
        this.minWidth = minWidth;
    }
}