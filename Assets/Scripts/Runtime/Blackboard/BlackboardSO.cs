using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Blackboard", menuName = "Blackboard/Blackboard", order = 0)]
public class BlackboardSO : ScriptableObject
{
    public string category;    
    public List<FactSO> facts = new List<FactSO>();

    private Dictionary<string, FactSO> factsDic = new Dictionary<string, FactSO>();

    /*public FactSO AddFact(FactType type)
    {
        FactSO newFact = FactFactory.CreateFact(type);
        
        facts.Add(newFact);
        factsDic[newFact.id] = newFact;

        return newFact;
    }

    public void RemoveFact(string factName)
    {
        if (factsDic.TryGetValue(factName, out FactSO fact))
        {
            facts.Remove(fact);
            factsDic.Remove(factName);
        }
        else
            throw new Exception("Can not remove fact because is not registered in the blackboard");
    }*/
    
    private FactSO GetFact(string factName)
    {
        if (factsDic.TryGetValue(factName, out FactSO fact))
            return fact;
        else
            throw new Exception("Fact is not registered in the blackboard");
    }
    
    public bool GetBool(string factName)
    {
        FactSO fact = GetFact(factName);

        if (fact is BoolFactSO boolFact)
            return boolFact.value;
        else
            throw new Exception("Fact is not of boolean type");
    }
    
    public int GetInt(string factName)
    {
        FactSO fact = GetFact(factName);

        if (fact is IntFactSO intFact)
            return intFact.value;
        else
            throw new Exception("Fact is not of int type");
    }
    
    public float GetFloat(string factName)
    {
        FactSO fact = GetFact(factName);

        if (fact is FloatFactSO floatFact)
            return floatFact.value;
        else
            throw new Exception("Fact is not of float type");
    }
    
    public string GetString(string factName)
    {
        FactSO fact = GetFact(factName);

        if (fact is StringFactSO stringFact)
            return stringFact.value;
        else
            throw new Exception("Fact is not of string type");
    }
}