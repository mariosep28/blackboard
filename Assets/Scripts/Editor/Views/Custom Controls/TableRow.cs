using System.Collections.Generic;
using System.Linq;
using UnityEngine.UIElements;

public class TableRow : VisualElement
{
    public TableRow(VisualElement row, List<TableColumn> columns)
    {
        AddToClassList("table-row");

        List<VisualElement> rowContent = row.Children().ToList();
        
        for (int i = 0; i < rowContent.Count; i++)
        {
            VisualElement cellContent = rowContent[i];

            TableCell tableCell = new TableCell(cellContent, columns[i].expandable, columns[i].minWidth);
            
            Add(tableCell);
        }
    }
}