using UnityEngine.UIElements;

public class TableCell : VisualElement
{
    private VisualElement cellContent;  
    
    public TableCell(VisualElement cellContent, bool expandable, int? minWidth = null)
    {
        Add(cellContent);
        
        AddToClassList("table-cell");
        
        if(expandable)
            AddToClassList("table-cell--expandable");
        else
            AddToClassList("table-cell--fixed");

        if (minWidth.HasValue)
        {
            style.minWidth = minWidth.Value;
        }
    }
}