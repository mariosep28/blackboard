using UnityEngine;

[CreateAssetMenu(fileName = "BoolFactSO", menuName = "Blackboard/Facts/Bool")]
public class BoolFactSO : FactSO
{
    public bool value;

    private void OnEnable()
    {
        factType = FactType.Bool;
    }
}